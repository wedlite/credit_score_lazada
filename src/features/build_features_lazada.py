from dotenv import find_dotenv, load_dotenv
import pandas as pd
import numpy as np
from pathlib import Path
import logging
import logging.config
from scipy import stats
from src.data.get_dataset_lazada import Dataset
from pandas.api.types import CategoricalDtype
from src.data import preprocess_lazada

load_dotenv(find_dotenv())
project_dir = Path(__file__).resolve().parents[2]
logging.config.fileConfig('{}/logging_config.ini'.format(project_dir))

class Features:
    """
    There are features that are both categorical and numeric used for model evaluation. 

    The binned attributes is to determine which feature you want to binned. If none provided then the feature
    will not be binned and only cut the using quantiles. If list of feature names provided then only the provided
    features will be binned. if "all" provided then all the numeric feature will be binned as stated in the eda notebooks.
    """

    def __init__(self, dataset, one_hot=[]):
        self.logger = logging.getLogger(__name__)
        self.dataset = dataset
        self.features = pd.DataFrame()
        self.one_hot = one_hot
        if 'default' in dataset:
            self.features['default'] = dataset['default']

    def bin_numeric_values(self, column_names, cut):
        """ binned the numeric values to desired binned size if contained in binned"""
#         if(column_names in self.binned or "all" in self.binned):
        # bin the numeric value into categorical value
        self.features[column_names] = pd.cut(
            self.features[column_names], cut)
        # get numeric code
        self.features[column_names] = self.features[column_names].cat.codes        
            
    #### from transactions data ####
    def build_transaction_data(self):

        # get application id
        self.features['application_id'] = self.dataset['application_id'].unique()
        self.features = self.features.set_index(['application_id'])

        # num transaction
        self.features = self.features.join(self.dataset.groupby(['application_id']).size() \
                        .reset_index() \
                        .rename(columns={0:'num_transaction'}).set_index(['application_id']))

        # average transaction amount
        self.dataset['amount'] = self.dataset['amount'].astype('float')
        self.features = self.features.join(self.dataset.groupby(['application_id'])['amount'].mean() \
                         .reset_index() \
                         .set_index(['application_id']))

        # count of failed, cancelled, and returned transactions
        temp = self.dataset.groupby(['application_id','orderItem_status']).size() \
                        .unstack() \
                        .reset_index().fillna(0) \
                        .set_index(['application_id'])
        
        self.features = self.features.join(temp)
        
        # count of shipment type
        self.features = self.features.join(self.dataset.groupby(['application_id','shipment_type']).size() \
                        .unstack() \
                        .reset_index().fillna(0) \
                        .set_index(['application_id']), on='application_id', lsuffix='_deliverystatus', rsuffix='_shipmenttype')

        # count of payment status
        self.features = self.features.join(self.dataset.groupby(['application_id','paid_status']).size() \
                        .unstack() \
                        .reset_index().fillna(0) \
                        .set_index(['application_id']))

        # count of fee name
        temp = self.dataset.groupby(['application_id','fee_name']).size() \
                        .unstack() \
                        .reset_index().fillna(0) \
                        .set_index(['application_id'])
        
        ## count of sold items
        self.features['num_sold_items'] = temp['Item Price Credit']
        
        ## count of returned items
        self.features['num_returned_items'] = temp['Reversal Item Price']
        
        temp_item_price_subsidy = temp['Item Price Subsidy']
        temp = temp.loc[:,temp.isin([' ','NULL',0]).mean() < .6]
        self.features = self.features.join(temp)
        
        # count of transaction type
        temp = self.dataset.groupby(['application_id','transaction_type']).size() \
                        .unstack() \
                        .reset_index().fillna(0) \
                        .set_index(['application_id'])
        
        ## count of subsidised items
        self.features['num_subsidised_items'] = temp['Other Services-Subsidy']+temp_item_price_subsidy
        
        temp = temp.loc[:,temp.isin([' ','NULL',0]).mean() < .6]
        self.features = self.features.join(temp)
        
        # count of bank names
        self.dataset['count_bank'] = self.dataset['payment_ref_id'].replace(regex=True,to_replace="([^[A-z]]*)",value='')
        self.dataset['count_bank'] = self.dataset['count_bank'].str.lower()
        
        self.dataset['count_bank'].replace(\
        {'ptbankcentralasiatbk':'bca',
         'bankmandiri':'mandiri',
         'ptbankmandiriperserotbk':'mandiri',
         'ptbankcimbniagatbk':'cimbniaga',
         'ptbankcentralasiatbk':'bca',
         'bankbni':'bni',
         'nodata':'nodata_countbank'
        }, inplace=True)
        
        temp = self.dataset.groupby(['application_id','count_bank']).size() \
                .unstack() \
                .reset_index().fillna(0) \
                .set_index(['application_id'])
        self.features = self.features.join(temp)
        
         # count of different bank types 
        temp = self.dataset.groupby(['application_id','count_bank']).size().reset_index(name='count')
        temp = temp.groupby(['application_id','count_bank']).size().unstack().fillna(0)
        temp['bank_type'] = temp.drop('nodata_countbank', axis=1).sum(axis=1)
        self.features = self.features.join(temp[['bank_type']])
        
        # count of shipping providers
        self.dataset['ship_provider_count'] = self.dataset['shipping_provider'].replace(regex=True,to_replace="([\W]*)",value='')
        self.dataset['ship_provider_count'] = self.dataset['ship_provider_count'].str.lower()
        
        self.dataset['ship_provider_count'].replace(\
        {'gojekmp':'gojek',
         'gojekmpbdo':'gojek',
         'jnecashless':'jne',
         'jnemp':'jne',
         'jnempmes':'jne',
         'jnempsub':'jne',
         'jneonlinebooking':'jne',
         'lexmp':'lex',
         'ncsmpmes':'ncs',
         'ninjavanmp':'ninja',
         '1':'nodata_countshipprovider',
         '80':'nodata_countshipprovider',
         'null':'nodata_countshipprovider',
         'nodata':'nodata_countshipprovider',
         'posindonesiamp':'posindonesia',
         'tikimp':'tiki',
         'tikimpmes':'tiki',
         'tikimpsub':'tiki'
        }, inplace=True)
        temp = self.dataset.groupby(['application_id','ship_provider_count']).size() \
                .unstack() \
                .reset_index().fillna(0) \
                .set_index(['application_id'])
        self.features = self.features.join(temp)
        
        # count of different shipping provider types
        temp = self.dataset.groupby(['application_id','ship_provider_count']).size().reset_index(name='count')
        temp = temp.groupby(['application_id','ship_provider_count']).size().unstack().fillna(0)
        temp['ship_provider_type'] = temp.drop('nodata_countshipprovider', axis=1).sum(axis=1)
        self.features = self.features.join(temp[['ship_provider_type']])
        
        # count of transactions per day
        self.dataset['transaction_date'] = self.dataset['transaction_date'].apply(pd.to_datetime)
        self.dataset['transaction_date_dayofweek'] = self.dataset['transaction_date'].dt.weekday_name
        temp = self.dataset.groupby(['application_id','transaction_date_dayofweek']).size() \
                .unstack() \
                .reset_index().fillna(0) \
                .set_index(['application_id'])
        self.features = self.features.join(temp)
        
        # count of transactions per weekday and weekend
        self.dataset['transaction_date_isweekend'] = np.where(self.dataset['transaction_date_dayofweek'].isin(['Sunday', 'Saturday']), 1,0)
        temp = self.dataset.groupby(['application_id','transaction_date_isweekend']).size() \
                .unstack() \
                .reset_index().fillna(0) \
                .set_index(['application_id'])
        temp.columns = ['num_is_weekday','num_is_weekend']
        self.features = self.features.join(temp)
        
        # count of yes/no if there is seller subsidy from lazada
        # subsidy is given to sellers with rating of more than 30%
        def check_subsidy(row):
            if row['fee_name'] == 'Item Price Subsidy' or row['transaction_type'] == 'Other Services-Subsidy':
                return 1
            else:
                return 0
        
        self.dataset['num_is_subsidy'] = self.dataset.apply(check_subsidy,axis=1)
        temp = self.dataset.groupby(['application_id'])['num_is_subsidy'].sum()
        self.features = self.features.join(temp)
        
        # count of yes/no if there is penalty on seller
        def check_penalty(row):
            if row['transaction_type'] == 'Refunds-Penalties':
                return 1
            else:
                return 0
           
        self.dataset['num_is_penalty'] = self.dataset.apply(check_penalty,axis=1)
        temp = self.dataset.groupby(['application_id'])['num_is_penalty'].sum()
        self.features = self.features.join(temp)
        
        # mean transaction per day
        df_trans = self.dataset.groupby(['application_id','transaction_date_dayofweek','transaction_date'])\
            .size().reset_index(name='trans_per_day')
        df_trans1 = df_trans.groupby(['application_id','transaction_date_dayofweek']).size()\
            .reset_index(name='count_days_all_trans').set_index('application_id')
        df_trans2 = df_trans.groupby(['application_id','transaction_date_dayofweek'])['trans_per_day'].sum()\
            .reset_index(name='count_trans_per_day').set_index('application_id')
        df_trans_all = pd.merge(df_trans1, df_trans2,  how='inner', left_on=['application_id','transaction_date_dayofweek'] \
                    , right_on = ['application_id','transaction_date_dayofweek'])
        df_trans_all['mean_trans_per_day'] = df_trans_all['count_trans_per_day']/df_trans_all['count_days_all_trans']
        
        df_trans_all = df_trans_all[['transaction_date_dayofweek','mean_trans_per_day']]
        df_trans_all = df_trans_all.reset_index().set_index(['application_id','transaction_date_dayofweek']).unstack()
        df_trans_all = df_trans_all['mean_trans_per_day'][['Monday','Tuesday','Wednesday',
                                                           'Thursday','Friday','Saturday','Sunday']]
        df_trans_all.columns = ['mean_trans_mon','mean_trans_tue','mean_trans_wed',
                        'mean_trans_thu','mean_trans_fri','mean_trans_sat','mean_trans_sun']

        self.features = self.features.join(df_trans_all)
        
        # number of unique product categories sold
        self.features = self.features.join(pd.DataFrame(self.dataset\
                                                        .groupby(['application_id','lazada_sku'])\
                                                        .size().reset_index()\
                                                        .groupby(['application_id'])['lazada_sku'].size()))
        
        # number of single purhcases
        self.features = self.features.join(pd.DataFrame(self.dataset\
                                                        .groupby(['application_id','order_no'])\
                                                        .size()\
                                                        .reset_index()\
                                                        .groupby(['application_id'])['order_no'].size()))
        
        # maybe do this on notebook? not at the code?
        # last 3 months - number of item sold
        
        
        # last 3 months - number of transaction
        
#         df_merge = pd.merge(transaction_data,application_pefindo_data[[\
#             'application_id','create_time','createdAt']], on ='application_id')
        
#         df_merge['days_before_application_createtime'] = (df_merge['create_time'] - df_merge['transaction_date']).dt.days
#         df_merge[(df_merge['days_before_application_createtime']>=0) \
#          & (df_merge['days_before_application_createtime']<=90)].groupby(['application_id']).size() \
#                         .reset_index() \
#                         .rename(columns={0:'num_transaction'}) \
#                         .set_index(['application_id'])
        
        # last 3 months - sum of transaction amount
        
        
        # slope/r squared of last 3 months
        
        
        # debt to income (sum of last 3 months) ratio
        
        
        # ratio of all counts
        self.features[['num_ratio_cancel',
                       'num_ratio_delivered',
                       'num_ratio_failed',
                       'num_ratio_returned',
                       'num_ratio_shipped',
                       'num_ratio_nodata_delivery',
                       'num_ratio_dropshipping',
                       'num_ratio_warehouse',
                       'num_ratio_nodata_shipment',
                       'num_ratio_paid',
                       'num_ratio_not_paid',
                       'num_ratio_adj_item_charge',
                       'num_ratio_adj_shipping_fee',
                       'num_ratio_item_price_credit',
                       'num_ratio_payment_fee',
                       'num_ratio_promo_charge_vouchers',
                       'num_ratio_reversal_item_price',
                       'num_ratio_ord_item_charges',
                       'num_ratio_ord_lazada_fees',
                       'num_ratio_other_services',
                       'num_ratio_refunds_item_charges',
                       'num_ratio_bca',
                        'num_ratio_bni',
                        'num_ratio_bri',
                        'num_ratio_cimbniaga',
                        'num_ratio_mandiri',
                        'num_ratio_nodata_countbank',
                        'num_ratio_permatabank',
                        'num_ratio_digitaldelivery2',
                         'num_ratio_gojek',
                         'num_ratio_jne',
                         'num_ratio_jtcashless',
                         'num_ratio_lex',
                         'num_ratio_ncs',
                         'num_ratio_ninja',
                         'num_ratio_nodata_countshipprovider',
                         'num_ratio_posindonesia',
                         'num_ratio_sellerfleet',
                         'num_ratio_sicepatmp',
                         'num_ratio_tiki',
                        'num_ratio_monday',
                       'num_ratio_tuesday',
                       'num_ratio_wednesday',
                       'num_ratio_thursday',
                       'num_ratio_friday',
                       'num_ratio_saturday',
                       'num_ratio_sunday',
                       'num_ratio_is_weekday',
                       'num_ratio_is_weekend',
                       'num_ratio_unique_categories',
                       'num_ratio_single_purchases',
                       'num_ratio_is_penalty',
                       'num_ratio_is_subsidy'\
#                        '',
                      ]] = \
        self.features[['Canceled',
                       'Delivered',
                       'Delivery Failed',
                       'Returned',
                       'Shipped',
                       'No Data_deliverystatus',
                       'Dropshipping',
                       'Own Warehouse',
                       'No Data_shipmenttype',
                       'Paid',
                       'Not paid',
                       'Adjustments Item Charge',
                       'Adjustments Shipping Fee',
                       'Item Price Credit',
                       'Payment Fee',
                       'Promotional Charges Vouchers',
                       'Reversal Item Price',
                       'Orders-Item Charges',
                       'Orders-Lazada Fees',
                       'Other Services-Services',
                       'Refunds-Item Charges',
                       'bca',
                        'bni',
                        'bri',
                        'cimbniaga',
                        'mandiri',
                        'nodata_countbank',
                        'permatabank',
                        'digitaldelivery2',
                         'gojek',
                         'jne',
                         'jtcashless',
                         'lex',
                         'ncs',
                         'ninja',
                         'nodata_countshipprovider',
                         'posindonesia',
                         'sellerfleet',
                         'sicepatmp',
                         'tiki',
                       'Monday',
                       'Tuesday',
                       'Wednesday',
                       'Thursday',
                       'Friday',
                       'Saturday',
                       'Sunday',
                       'num_is_weekday',
                       'num_is_weekend',
                       'lazada_sku',
                       'order_no',
                       'num_is_penalty',
                       'num_is_subsidy'\
                      ]] \
            .div(self.features['num_transaction'],axis=0)
        
        # num of paid transaction
        self.features = self.features.reset_index().rename(columns={'amount':'mean_trans_amount',
                                                                    'Canceled':'num_cancelled',
                                                                   'Delivered':'num_delivered',
                                                                   'Delivery Failed':'num_delivery_failed',
                                                                   'No Data_deliverystatus':'num_nodata_deliverystatus',
                                                                   'Returned':'num_returned',
                                                                   'Shipped':'num_shipped',
                                                                   'Dropshipping':'num_dropshipping',
                                                                   'No Data_shipmenttype':'num_nodata_shipmenttype',
                                                                   'Own Warehouse':'num_warehouse',
                                                                   'Not paid':'num_notpaid',
                                                                   'Paid':'num_paid',
                                                                   'Adjustments Item Charge':'num_adj_item_charge',
                                                                   'Adjustments Shipping Fee':'num_adj_shipping_fee',
                                                                   'Item Price Credit':'num_item_price_credit',
                                                                   'Payment Fee':'num_payment_fee',
                                                                    'Promotional Charges Vouchers':'num_promo_vouchers',
                                                                    'Reversal Item Price':'num_rev_item_price',
                                                                    'Sponsored Product Fee':'num_sponsor_product_fee',
                                                                    'Orders-Item Charges':'num_ord_item_charges',
                                                                    'Orders-Lazada Fees':'num_lazada_fees',
                                                                    'Other Services-Services':'num_other_services',
                                                                   'Refunds-Item Charges':'num_refunds_item_charges',
                                                                    'bca':'num_bca',
                                                                    'bni':'num_bni',
                                                                    'bri':'num_bri',
                                                                    'cimbniaga':'num_cimbniaga',
                                                                    'mandiri':'num_mandiri',
                                                                    'nodata_countbank':'num_nodata_countbank',
                                                                    'permatabank':'num_permatabank',
                                                                    'digitaldelivery2':'num_digitaldelivery2',
                                                                     'gojek':'num_gojek',
                                                                     'jne':'num_jne',
                                                                     'jtcashless':'num_jtcashless',
                                                                     'lex':'num_lex',
                                                                     'ncs':'num_ncs',
                                                                     'ninja':'num_ninja',
                                                                     'nodata_countshipprovider':'num_nodata_countshipprovider',
                                                                     'posindonesia':'num_posindonesia',
                                                                     'sellerfleet':'num_sellerfleet',
                                                                     'sicepatmp':'num_sicepatmp',
                                                                     'tiki':'num_tiki',
                                                                     'Monday':'num_monday',
                                                                     'Tuesday':'num_tuesday',
                                                                   'Wednesday':'num_wednesday',
                                                                   'Thursday':'num_thursday',
                                                                   'Friday':'num_friday',
                                                                   'Saturday':'num_saturday',
                                                                   'Sunday':'num_sunday',
                                                                   'isweekday':'num_isweekday',
                                                                   'isweekend':'num_isweekend',
                                                                   'lazada_sku':'num_unique_categories',
                                                                   'order_no':'num_single_purchases'})
#                                                                    '':'',
#                                                                    '':'',
#                                                                    '':'',
#                                                                    '':'',
#                                                                    '':'',
#                                                                    '':'',
#                                                                    '':'',
#                                                                    '':'',})
        
        self.features = self.features.reset_index(drop=True)

    #### from application and pefindo data ####
    def build_application_pefindo_data(self):
        self.features['domicile_ownership'] = self.dataset['domicile_ownership'].replace({'milik-sendiri' : 1, 
                                             'orang-tua' : 2, 'others' : 3, 'sewa' : 4}).astype(int)
        self.features['domicile_stay_period'] = self.dataset['domicile_stay_period'].astype('float')
        self.features['loan_amount'] = self.dataset['loan_amount']
        self.features['loan_duration'] = self.dataset['loan_duration']
    
    
    #### from pefindo data ####
    def build_pefindo(self):
        self.logger.info('Building pefindo..')
        # regroup pefindo grade
        self.features.loc[self.dataset['pefindo_grade'].str[0].str.contains('D|E'), 'cat_pefindo_grade'] = 'C'
        self.features.loc[self.dataset['pefindo_grade'].str[0].str.contains('C'), 'cat_pefindo_grade'] = 'B'
        self.features.loc[self.dataset['pefindo_grade'].str.strip().isin(['B2','B3']), 'cat_pefindo_grade'] = 'B'
        self.features.loc[self.dataset['pefindo_grade'].str.strip().isin(['B1']), 'cat_pefindo_grade'] = 'A'
        self.features['cat_pefindo_grade'] = self.features['cat_pefindo_grade'].fillna('No Data')
        grade_replace = {
            'A': 0,
            'B': 1,
            'C': 2,
            'No Data': 3
        }
        self.features['cat_pefindo_grade'] = self.features['cat_pefindo_grade'].map(grade_replace)

        # get pefindo score
        self.features['num_pefindo_score'] = self.dataset['pefindo_score'].astype(int)
        self.features.loc[self.features['num_pefindo_score'] == 999, "num_pefindo_score"] = 0
        self.bin_numeric_values('num_pefindo_score', [-1,0,200,600,620,640,658,670,700,float("inf")])

        # transform due amount to category
        self.dataset.loc[:, 'dueAmount'] = self.dataset['dueAmount'].astype(float)
        self.features['cat_due_amount'] = self.dataset['dueAmount']
        self.features.loc[self.dataset['dueAmount'] > 0, 'cat_due_amount'] = 1

        # transform outstanding amount to category
        self.dataset.loc[:, 'outstandingAmount'] = self.dataset['outstandingAmount'].astype(float)
        self.features['cat_outstanding_amount'] = self.dataset['outstandingAmount']
        self.features.loc[self.dataset['outstandingAmount'] > 0, 'cat_outstanding_amount'] = 1
    
        # category of credit classification
        self.features['cat_credit_classification'] = self.dataset['credit_classification'].replace({'No Data' : -1, 'NonMSME' : 2, 'NotSpecified' : 3, 'OtherSmallEnt' : 4, 'SpecificGuarantorMicroEnt': 5}).astype(int)
        
        # value of noncash collaterals
        self.features['num_noncash_collaterals_value'] = self.dataset['noncash_collaterals_value']
        
        # worst past due days in the last 12 months
        self.features['num_worst_dpd_12'] = self.dataset['worst_dpd_12']
        
        # number of subscribers in the last 12 months
        self.features['num_subscribers_last12'] = self.dataset['num_subscribers_last12']
        
        # number of open contracts by debtor
        self.features['num_debtor_open_contracts'] = self.dataset['debtor_open_contracts']
        
        # number of closed contracts by debtor
        self.features['num_debtor_closed_contracts'] = self.dataset['debtor_closed_contracts']
        
        # number of inquiries in the last 24 months
        self.features['num_inquiry_24'] = self.dataset['num_inquiry_24']
        
    
    def build_all_application_pefindo_features(self):
        self.build_pefindo()
        self.features['application_id'] = self.dataset['application_id']
        self.logger.info('Application pefindo features building finished')
        return self.features
    
    def build_all_transaction_features(self):
        self.build_transaction_data()
        self.logger.info('Transaction features building finished')
        return self.features

def olr(y, sigma=0.1, months=12, norm=False):
    """ train ordinary least squares for cashflow to know the slope and intercept """
    x = np.array(range(months, 0, -1))
    if(norm):
        y = np.array(y[:months]) / max(y)
    else:
        y = np.array(y) / 1000000
    slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
    r_squared = r_value ** 2
    # number of months that the revenue are lower than the linear limit
    lower_limit = (intercept - sigma) + (slope * x)
    down_rate = np.sum(lower_limit > y)
    # number of months that the revenue are higher than the linear limit
    upper_limit = (intercept + sigma) + (slope * x)
    up_rate = np.sum(upper_limit < y)
    stable_rate = months - (up_rate + down_rate)
    return [slope, intercept, r_squared, down_rate, up_rate, stable_rate]

def approx_transaction(row):
    cash_arr = np.array(row['cashflow_history'])
    trans_arr = np.array(row['transaction_history'])
    row['approx_transactions'] = np.nan_to_num(cash_arr / trans_arr)
    return row

def main():
    dataset = Dataset()
    train_data, test_data = dataset.get_dataset()
    features = Features(train_data)
    dataset.get_clean_dataset()


if __name__ == '__main__':
    # find .env conf file automagically
    main()