from dotenv import find_dotenv, load_dotenv
import logging
import logging.config
import pandas as pd
import numpy as np
from pathlib import Path
from multiprocessing import Pool, cpu_count
import requests
import uuid
import os

load_dotenv(find_dotenv())
project_dir = Path(__file__).resolve().parents[2]
logging.config.fileConfig('{}/logging_config.ini'.format(project_dir))

def get_valid_npwp(npwp):
    """ get valid npwp on real time in the cloud functions (sse pajak) """
    npwp_name = np.nan
    if(len(npwp) > 0):
        query = {'npwp':npwp}
        r = requests.get('https://us-central1-taralite-ekyc.cloudfunctions.net/checkNpwp', params=query)
        if (len(r.text) > 0):
            results = r.json()
            npwp_name = results[0]['nama'] if len(results) > 0 else np.nan
    return npwp_name

def get_pefindo_data(row):
    """ get pefindo data on real time in the cloud functions """
    logger = logging.getLogger(__name__)
    query = {'ktp':row['ktp_no'], 'dateRequest': row['create_time']}
    pef_user = os.getenv('PEFINDO_USER')
    pef_pass = os.getenv('PEFINDO_PASS')
    r = requests.post('https://pefindo.taralite.com/getpefindoscoring', data=query, auth=(pef_user, pef_pass))
    logger.debug("pefindo response code {}".format(r.status_code))
    if(len(r.text) > 0 and r.status_code == 200):
        results = r.json()
        if('multiple' not in results['status'] and 'no data' not in results['status'] and 'no transaction' not in results['data']):
            if(len(results['data']) > 0):
                results = results['data']
                row['pefindo_grade'] = results['grade'] if results['grade'] != 'N/A' else np.nan 
                row['dueAmount'] = results['dueAmount'] if results['dueAmount'] != 'N/A' else np.nan
                row['pefindo_score'] = results['score'] if results['score'] != 'N/A' else np.nan
                row['outstandingAmount'] = results['outstandingAmount'] if results['outstandingAmount'] != 'N/A' else np.nan
    return row

def get_required_colums(data, required_columns):
    """ quick and dirty fix for some missing columns in datastore """
    missing_col = set(required_columns) - set(data.columns)
    if(len(missing_col) > 0):
        for col in missing_col:
            data[col] = np.nan
    data = data[required_columns]
    return data

def clean_seller_transaction_data(data,old_or_new):
    """clean old and seller transaction data"""
    logger = logging.getLogger(__name__)
    logger.info('Seller transaction data cleaning started')
    
    data = data.drop(data[data['transactions'].map(lambda d: len(d)) < 1].index).reset_index(drop=True)
    
    # extract the transaction table into individual rows
    def extract_transactions(rows):
        data_extract = pd.DataFrame(rows['transactions'])
        data_extract['application_id'] = rows['application_id']
        return data_extract
    
    logger.info('extract seller transaction data')
    data = pd.concat(data.apply(extract_transactions, axis=1).tolist()).reset_index(drop=True)
    data = data.drop(columns=['__key__'])
    data = data.drop_duplicates().reset_index(drop=True)
    data = data.replace(r'^\s*$', np.nan, regex=True)
    logger.info('Seller transaction data extraction started')
    # save the cleaned data into cache
    cache = '{project_dir}/data/external/data_{old_or_new}_clean.csv'.format(project_dir=project_dir,old_or_new=old_or_new)
    data.to_csv(cache)
    logger.info('Transaction data cleaning completed')
    return data

def default_fill_seller_transaction_data(data):
    logger = logging.getLogger(__name__)
    logger.info('Replace numerical nan with -999')
    # replace numerical nan with -999
    data[['VAT_in_amount'
        , 'amount'
    ]] = data[['VAT_in_amount'
        , 'amount'
    ]].apply(lambda x: x.fillna(-999))
    
    logger.info('Replace categorical nan with No Data')
    # replace categorical nan to No Data
    data[['WHT_included_in_amount'
        ,'WHT_amount'
        ,'comment'
        ,'details'
        ,'fee_name'
        ,'lazada_sku'
        ,'orderItem_no'
        ,'orderItem_status'
        ,'order_no'
        ,'paid_status'
        ,'payment_ref_id'
        ,'reference'
        ,'seller_sku'
        ,'shipment_type'
        ,'shipping_provider'
        ,'shipping_speed'
        ,'statement'
        ,'transaction_date'
        ,'transaction_number'
        ,'transaction_type'
    ]] = data[['WHT_included_in_amount'
        ,'WHT_amount'
        ,'comment'
        ,'details'
        ,'fee_name'
        ,'lazada_sku'
        ,'orderItem_no'
        ,'orderItem_status'
        ,'order_no'
        ,'paid_status'
        ,'payment_ref_id'
        ,'reference'
        ,'seller_sku'
        ,'shipment_type'
        ,'shipping_provider'
        ,'shipping_speed'
        ,'statement'
        ,'transaction_date'
        ,'transaction_number'
        ,'transaction_type'
    ]].fillna('No Data')
    # save the cleaned data into cache
    cache = '{project_dir}/data/external/data_all_clean.csv'.format(project_dir=project_dir)
    data.to_csv(cache)
    logger.info('Transaction data fillna completed')
    return data


def merge_transaction_data(data_old,data_new):
    logger = logging.getLogger(__name__)
    data = pd.concat([data_old,data_new],axis=0).reset_index(drop=True)
    data = data.drop_duplicates().reset_index(drop=True)
    
    # change the column data type to datetime type
    data['transaction_date'] = pd.to_datetime(data['transaction_date'])
    # build monthly bins with 3 years range    
    bins_dt = pd.date_range('2016-01-01',freq='1M',periods=48)
    bins_str = bins_dt.astype(str).values
    labels = ['({}, {}]'.format(bins_str[i-1], bins_str[i]) for i in range(1, len(bins_str))]
    data['transaction_date_cat'] = pd.cut(
    data['transaction_date'].astype(np.int64)//10**9,
                   bins=bins_dt.astype(np.int64)//10**9,
                   labels=labels)
    
    cache = '{project_dir}/data/external/data_all_clean.csv'.format(project_dir=project_dir)
    data.to_csv(cache)
    logger.info('Transaction data merging completed')
    return data

    
def clean_application_pefindo_data(data):
    """ clean pefindo data for analysis """
    
    logger = logging.getLogger(__name__)

    logger.info('Application and Pefindo data cleaning started')
    data = data.replace('', np.nan)
    
    # replace numerical nan with -999
    data[['num_inquiry_24'
        , 'noncash_collaterals_value'
        , 'worst_dpd_12'
        , 'num_subscribers_last12'
        , 'outstandingAmount'
        , 'dueAmount'
        , 'pefindo_score'
        , 'pefindo_prob_default'
        , 'debtor_open_contracts'
        , 'debtor_closed_contracts'
    ]] = data[['num_inquiry_24'
    , 'noncash_collaterals_value'
    , 'worst_dpd_12'
    , 'num_subscribers_last12'
    , 'outstandingAmount'
    , 'dueAmount'
    , 'pefindo_score'
    , 'pefindo_prob_default'
    , 'debtor_open_contracts'
    , 'debtor_closed_contracts'
    ]].apply(lambda x: x.fillna(-999))

    # replace categorical nan to No Data
    data[['valid_npwp_name'
        ,'credit_classification'
        ,'pefindo_grade'
        ,'pefindo_trend',
    ]] = data[['valid_npwp_name'
        ,'credit_classification'
        ,'pefindo_grade'
        ,'pefindo_trend',
    ]].fillna('No Data')

    # change Pefindo Grade XX to No Data
    mask = data['pefindo_grade'].str.strip().isin(['XX'])
    data.loc[mask, 'pefindo_grade'] = 'No Data'
    logger.info('Pefindo data cleaning completed')
    return data
    