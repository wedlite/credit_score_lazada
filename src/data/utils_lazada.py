from dotenv import find_dotenv, load_dotenv
from google.cloud import bigquery, datastore
from pandas_gbq import gbq
import logging
import logging.config
from sqlalchemy import create_engine
import sqlalchemy.exc as sqlalchemy_exc
import pandas as pd
from pathlib import Path
import os
from pandas.io.json import json_normalize

# find .env automagically
load_dotenv(find_dotenv())

project_dir = Path(__file__).resolve().parents[2]
logging.config.fileConfig('{}/logging_config.ini'.format(project_dir))

def query_from_pg_admin(query, retries=5):
    """ Query data from admin service """
    logger = logging.getLogger(__name__)
    data = []
    logger.info('Making admin service database connection')

    while retries > 0:
        try:
            # admin-service database connections
            db_host = os.getenv('DB_PRD_HOST')
            db_port = os.getenv('DB_PRD_PORT')
            db_user = os.getenv('DB_PRD_USER')
            db_pass = os.getenv('DB_PRD_PASS')
            db_name = 'admin-service'
            conn_string = 'postgresql://{}:{}@{}:{}/{}'.format(
                db_user, db_pass, db_host, db_port, db_name)

            engine = create_engine(conn_string)
            logging.info("Get data from admin service into csv file")
            data = pd.read_sql(query, engine)
            engine.dispose()
            retries = 0
        except sqlalchemy_exc.SQLAlchemyError:
            retries -= 1
            logger.error("Can't connect to admin service database.", exc_info=True)
            return False
    logger.info('query completed')
    return data

def query_from_datastore(kind, filters={}, retries=5):
    """ query loan proposals from datastore """
    logger = logging.getLogger(__name__)

    logger.info('Query from datastore')
    logger.debug(filters)
    client  = datastore.Client()
    query   = client.query(kind=kind, namespace = 'lazada')
    for filter_key, filter_value in filters.items():
        query.add_filter(filter_key, '=', int(filter_value))
    data    = list(query.fetch())
    data    = json_normalize(data)
    logger.info('Query completed')

    return data

def query_from_biggquery(query, retries=5):
    """ query loan proposals from bigquery """
    logger = logging.getLogger(__name__)
    data = []
    logger.info('Making bigquery connection')

    # sometimes google return 500 error results
    while retries > 0: 
        try:
            # use native gcp client library
            client = bigquery.Client()
            job_config = bigquery.QueryJobConfig()
            data = client.query(query, 
                job_config=job_config).result().to_dataframe()
            retries = 0
        except:
            retries -= 1
            logger.error('error connecting', exc_info=True)
            logger.info('retrying the queries')
    logger.info('Query completed')

    return data
