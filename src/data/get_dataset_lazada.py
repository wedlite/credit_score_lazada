from dotenv import find_dotenv, load_dotenv
import pandas as pd
import numpy as np
import logging
import logging.config
import os
from pathlib import Path
from src.data import preprocess_lazada, utils_lazada
from src.data import utils_lazada
from pandas.io.json import json_normalize
from sklearn.model_selection import StratifiedShuffleSplit
import pickle

load_dotenv(find_dotenv())
project_dir = Path(__file__).resolve().parents[2]
logging.config.fileConfig('{}/logging_config.ini'.format(project_dir))

class Dataset:
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.admin_data = None
        self.application_pefindo_data = None
        self.seller_old_transaction_data = None
        self.seller_new_transaction_data = None
        self.seller_clean_transaction_data = None
        self.clean_data = None
        self.npwp = None
        self.train_dataset = None
        self.test_dataset = None
        self.project_dir = project_dir
        self.cache = {
            'admin_data': '{}/data/raw/loan_default.csv'.format(self.project_dir),
            'application_pefindo_data': '{}/data/raw/application_pefindo_data.pkl'.format(self.project_dir),
            'seller_old_transaction_data': '{}/data/raw/seller_old_transaction_data.pkl'.format(self.project_dir),
            'seller_new_transaction_data': '{}/data/raw/seller_new_transaction_data.pkl'.format(self.project_dir)
        }
    def get_admin_service_data(self):
        """ 
        Get loans' data with default information from admin-service database and save into loan_default.csv.
        """
        # cache's filename
        if(Path(self.cache['admin_data']).exists() == False):
            admin_service_query = """
                SELECT all_loans.*,"LazadaLoanProposals".application_id
                FROM 
                (
                    SELECT 
                        id, 
                        amount, 
                        status, 
                        ever_late_days,
                        interest_rate,
                        "createdAt",
                        "disbursed_at",
                        proposal_id,
                        sum(invoice_amount) as invoice_amount, 
                        sum(invoice_paid) as invoice_paid,
                        sum(invoice_amount - invoice_paid) as amount_needed_to_be_paid,
                        COUNT(CASE WHEN invoice_status = 'paid' then 1 ELSE NULL END) as num_invoice_paid, 
                        COUNT(CASE WHEN invoice_status = 'unpaid' then 1 ELSE NULL END) as num_invoice_unpaid
                    FROM
                    (
                        SELECT 
                            l.id,
                            l.amount,
                            l.status,
                            l.interest_rate,
                            l."createdAt",
                            l."disbursed_at",
                            l.proposal_id,
                            "LoanInvoices".amount as invoice_amount, 
                            "LoanInvoices".status as invoice_status,
                            "LoanInvoices".paid as invoice_paid, 
                            ever_late_loans.ever_late_days
                        FROM
                        (
                            SELECT 
                                loan_id, 
                                max( late_days ) as ever_late_days
                            FROM
                            (
                                SELECT 
                                    loan_id,
                                    li.paid, COUNT(*) as late_days
                                FROM "LoanInvoices" li
                                INNER JOIN "Penalties" p on li.id = p.loan_invoice_id
                                GROUP BY loan_invoice_id, loan_id, li.amount, li.paid
                                ORDER BY loan_id
                            ) ever_late_invoice
                            GROUP BY loan_id
                            ORDER BY loan_id
                        ) ever_late_loans
                            RIGHT JOIN "Loans" l on l.id = loan_id
                            JOIN "LoanInvoices" on "LoanInvoices".loan_id = l.id
                        order by l.id
                    ) t
                    GROUP by
                        id, 
                        "createdAt",
                        "disbursed_at",
                        proposal_id,
                        amount, 
                        status,
                        interest_rate, 
                        ever_late_days
                )all_loans 
                INNER JOIN "LoanProposals" ON "LoanProposals".id = all_loans.proposal_id
                INNER JOIN "LazadaLoanProposals" ON "LazadaLoanProposals".loan_proposal_id = "LoanProposals".id
                WHERE product_id = 3 or product_id = 29 or product_id = 44
            """
            data = utils_lazada.query_from_pg_admin(admin_service_query)
            data = data.reset_index(drop=True)
            data.to_csv(self.cache['admin_data'], index=False)
            self.logger.info('Download from admin service completed')
        else:
            self.logger.info('Use cache for admin service data')
            data = pd.read_csv(self.cache['admin_data'])
         # fill ever late days to 0
        data['ever_late_days'] = data['ever_late_days'].fillna(0)
        # remove undetermined loans
        # data = data.drop(data.query('status == "disbursed" and ever_late_days <= 90').index)
        # data = data.drop(data.query('status == "refinanced"').index)
        # label default using dpd > 30
        data['default'] = 0
        data.loc[data.query('status == "disbursed" and ever_late_days > 30').index, 'default'] = 1
        # data.loc[data.query('status == "closed" and ever_late_days > 60').index, 'default'] = 1
        self.admin_data = data
        return self.admin_data

    def get_application_pefindo_data(self):
        """
        Get and clean lazada loan proposal's data into a csv file. It contains Pefindo and application data from BigQuery
        """
        # cache's filename
        if(Path(self.cache['application_pefindo_data']).exists() == False):
            query = """
                SELECT
                    lazada_apply_data.application_id,
                    lazada_apply_data.domicile_ownership,
                    lazada_apply_data.domicile_stay_period,
                    lazada_apply_data.npwp_number as npwp_no,
                    lazada_apply_data.loan_duration,
                    lazada_apply_data.create_time,
                    lazada_apply_data.loan_amount,
                    lazada_apply_data.gender,
                    lazada_apply_data.birth_date,
                    lazada_apply_data.marital_status,
                    lazada_apply_data.ktp_number as ktp_no,
                    CASE
                        WHEN Report.Inquiries.Summary.NumberOfInquiriesLast24Months IS NOT NULL 
                        THEN Report.Inquiries.Summary.NumberOfInquiriesLast24Months
                        ELSE Report.GetCustomReportResult.Inquiries.Summary.NumberOfInquiriesLast24Months
                    END AS num_inquiry_24,
                    CASE
                        WHEN Report.Collaterals.Summary.TotalValueOfNonCashCollaterals.LocalValue IS NOT NULL 
                        THEN Report.Collaterals.Summary.TotalValueOfNonCashCollaterals.LocalValue
                        ELSE Report.GetCustomReportResult.Collaterals.Summary.TotalValueOfNonCashCollaterals.LocalValue
                    END AS noncash_collaterals_value,
                    CASE
                        WHEN Report.GetCustomReportResult.Contracts.ContractList.Contract[ORDINAL(1)].CreditClassification IS NOT NULL 
                        THEN Report.GetCustomReportResult.Contracts.ContractList.Contract[ORDINAL(1)].CreditClassification
                        ELSE Report.Contracts.ContractList.Contract[ORDINAL(1)].CreditClassification
                    END AS credit_classification,
                    CASE
                        WHEN Report.Dashboard.PaymentsProfile.WorstPastDueDaysForLast12Months IS NOT NULL 
                        THEN Report.Dashboard.PaymentsProfile.WorstPastDueDaysForLast12Months
                        ELSE Report.GetCustomReportResult.Dashboard.PaymentsProfile.WorstPastDueDaysForLast12Months
                    END AS worst_dpd_12,
                    CASE
                        WHEN Report.Dashboard.Inquiries.SubscribersInLast12Months IS NOT NULL 
                        THEN Report.Dashboard.Inquiries.SubscribersInLast12Months
                        ELSE Report.GetCustomReportResult.Dashboard.Inquiries.SubscribersInLast12Months
                    END AS num_subscribers_last12,
                    CASE
                        WHEN Report.ContractSummary.Debtor.OutstandingAmountSum.entity.LocalValue IS NOT NULL 
                        THEN Report.ContractSummary.Debtor.OutstandingAmountSum.entity.LocalValue
                        ELSE Report.GetCustomReportResult.ContractSummary.Debtor.OutstandingAmountSum.LocalValue
                    END AS outstandingAmount,
                    CASE
                        WHEN Report.ContractSummary.Debtor.PastDueAmountSum.entity.LocalValue IS NOT NULL 
                        THEN Report.ContractSummary.Debtor.PastDueAmountSum.entity.LocalValue
                        ELSE Report.GetCustomReportResult.ContractSummary.Debtor.PastDueAmountSum.LocalValue
                    END AS dueAmount,
                    CASE
                        WHEN Report.CIP.RecordList.Record[ORDINAL(1)].grade IS NOT NULL 
                        THEN Report.CIP.RecordList.Record[ORDINAL(1)].grade
                        ELSE Report.GetCustomReportResult.CIP.RecordList.Record[ORDINAL(1)].grade
                    END AS pefindo_grade,
                    CASE
                        WHEN Report.CIP.RecordList.Record[ORDINAL(1)].score IS NOT NULL 
                        THEN Report.CIP.RecordList.Record[ORDINAL(1)].score
                        ELSE Report.GetCustomReportResult.CIP.RecordList.Record[ORDINAL(1)].score
                    END AS pefindo_score,
                    CASE
                        WHEN Report.CIP.RecordList.Record[ORDINAL(1)].trend IS NOT NULL 
                        THEN Report.CIP.RecordList.Record[ORDINAL(1)].trend
                        ELSE Report.GetCustomReportResult.CIP.RecordList.Record[ORDINAL(1)].trend
                    END AS pefindo_trend,
                    CASE
                        WHEN Report.CIP.RecordList.Record[ORDINAL(1)].ProbabilityOfDefault IS NOT NULL 
                        THEN Report.CIP.RecordList.Record[ORDINAL(1)].ProbabilityOfDefault
                        ELSE Report.GetCustomReportResult.CIP.RecordList.Record[ORDINAL(1)].ProbabilityOfDefault
                    END AS pefindo_prob_default,
                    CASE
                        WHEN Report.GetCustomReportResult.ContractSummary.SectorInfoList.SectorInfo[ORDINAL(1)].DebtorOpenContracts IS NOT NULL 
                        THEN Report.GetCustomReportResult.ContractSummary.SectorInfoList.SectorInfo[ORDINAL(1)].DebtorOpenContracts
                        ELSE Report.ContractSummary.SectorInfoList.SectorInfo[ORDINAL(1)].DebtorOpenContracts
                    END AS debtor_open_contracts,
                    CASE
                        WHEN Report.GetCustomReportResult.ContractSummary.SectorInfoList.SectorInfo[ORDINAL(1)].DebtorClosedContracts IS NOT NULL 
                        THEN Report.GetCustomReportResult.ContractSummary.SectorInfoList.SectorInfo[ORDINAL(1)].DebtorClosedContracts
                        ELSE Report.ContractSummary.SectorInfoList.SectorInfo[ORDINAL(1)].DebtorClosedContracts
                    END AS debtor_closed_contracts
                FROM
                loan_proposal.lazada_application_data AS lazada_apply_data
                LEFT JOIN
                credit_score.pefindo_single pef_s
                ON
                lazada_apply_data.ktp_number = pef_s.__key__.name
                LEFT JOIN
                credit_score.pefindo_report pef_r
                ON
                pef_s.Pefindoid = pef_r.__key__.name
                WHERE
                  program_name = 'Dana Tara'
            """    
            data = utils_lazada.query_from_biggquery(query)
            data = data.reset_index(drop=True)
            if(len(data) > 0):
                # save as pickle due to complex type data
                with open(self.cache['application_pefindo_data'], 'wb') as cache_file:
                    pickle.dump(data, cache_file)
        else:
            self.logger.info('Use cache for application pefindo data!!!!???')
            with open(self.cache['application_pefindo_data'], 'rb') as cache_file:
                data = pickle.load(cache_file)
        self.application_pefindo_data = data
        return self.application_pefindo_data

    def get_old_transaction_data(self):
        """
        Get lazada old seller transaction data from BigQuery. Only several ids are selected because the memory is
        not enough for entire query
        """
        # cache's filename
        if(Path(self.cache['seller_old_transaction_data']).exists() == False):
            query = """
                SELECT
                    lz_seller.seller_id,
                    lz_seller.short_code,
                    lz_apply_data.application_id,
                    lz_seller_trans_old.timestamp,
                    lz_seller_trans_old.transactions
                FROM
                    `taralite-apply.loan_proposal_asia.LZSeller` AS lz_seller
                JOIN
                    `taralite-apply.loan_proposal_asia.LZSellerTransaction` AS lz_seller_trans_old
                ON
                    lz_seller_trans_old.seller_id = lz_seller.seller_id
                JOIN
                    `taralite-apply.loan_proposal_asia.lazada_application_data` AS lz_apply_data
                ON
                    lz_apply_data.seller_id = lz_seller.short_code
                RIGHT JOIN
                    `taralite-apply.loan_proposal_asia.wanted_app_id`
                ON
                    cast(`taralite-apply.loan_proposal_asia.wanted_app_id`.application_id  as int64) = lz_apply_data.application_id
            """
            data = utils_lazada.query_from_biggquery(query)
            data = data.reset_index(drop=True)
            if(len(data) > 0):
                # save as pickle due to complex type data
                with open(self.cache['seller_old_transaction_data'], 'wb') as cache_file:
                    pickle.dump(data, cache_file)
        else:
            self.logger.info('Use cache for seller old transaction data')
            with open(self.cache['seller_old_transaction_data'], 'rb') as cache_file:
                data = pickle.load(cache_file)
        self.seller_old_transaction_data = data
        return self.seller_old_transaction_data

    def get_new_transaction_data(self):
        """
        Get lazada new seller transaction data from BigQuery. Only several ids are selected because the memory is
        not enough for entire query
        """
        # cache's filename
        if(Path(self.cache['seller_new_transaction_data']).exists() == False):
            query = """
                SELECT
                    lz_seller.seller_id,
                    lz_seller.short_code,
                    lz_apply_data.application_id,
                    lz_seller_trans_new.timestamp,
                    lz_seller_trans_new.transactions
                FROM
                    `taralite-apply.loan_proposal_asia.LZSeller` AS lz_seller
                JOIN
                    `taralite-apply.loan_proposal_asia.lazadaSellerTransactions` AS lz_seller_trans_new
                ON
                    lz_seller_trans_new.seller_id = lz_seller.seller_id
                JOIN
                    `taralite-apply.loan_proposal_asia.lazada_application_data` AS lz_apply_data
                ON
                    lz_apply_data.seller_id = lz_seller.short_code
                RIGHT JOIN
                    `taralite-apply.loan_proposal_asia.wanted_app_id`
                ON
                    cast(`taralite-apply.loan_proposal_asia.wanted_app_id`.application_id  as int64) = lz_apply_data.application_id
            """
            data = utils.query_from_biggquery(query)
            data = data.reset_index(drop=True)
            if(len(data) > 0):
                # save as pickle due to complex type data
                with open(self.cache['seller_new_transaction_data'], 'wb') as cache_file:
                    pickle.dump(data, cache_file)
        else:
            self.logger.info('Use cache for seller new transaction data')
            with open(self.cache['seller_new_transaction_data'], 'rb') as cache_file:
                data = pickle.load(cache_file)
        self.seller_new_transaction_data = data
        return self.seller_new_transaction_data

    def get_npwp_data(self):
        """
        get npwp data from dataframe
        """
        query = "select npwp.npwp as npwp_no, nama as valid_npwp_name from credit_score.npwp"
        data = utils_lazada.query_from_biggquery(query)
        self.npwp = data
        self.logger.info('get npwp data!!!!')
        return self.npwp

    def get_split_dataset(self, random_state=1234):
        """ get cleaned dataset and split into training and test dataset """
        self.logger.info('!!??try the function get dataset??!!')
        self.get_clean_dataset()
        self.logger.info('!now trying get clean dataset!')
        dataset = self.clean_data
        ShuffleSplit = StratifiedShuffleSplit(n_splits=1, 
                                      test_size=0.25, 
                                      random_state=random_state)
        # split the dataset with balanced target
        # sklearn required number of rows and target to split
        for train_index, test_index in ShuffleSplit.split(np.zeros(len(dataset)), 
                                                        dataset['default']):
            self.train_dataset = dataset.iloc[train_index]
            self.test_dataset  = dataset.iloc[test_index] 
        return self.train_dataset, self.test_dataset

    def get_clean_dataset(self):
        """
        Merge tokopedia loan proposal's and admin services data by application id into dataset that 
        are ready to be analyzed for credit scoring model.
        """
        # get data
        self.get_admin_service_data()
        self.get_application_pefindo_data()
        self.get_old_transaction_data()
        self.get_new_transaction_data()
        self.get_npwp_data()
        # remove null paid and unpaid
        # self.admin_data = self.admin_data[~(self.admin_data['paid'].isnull()) | ~(self.admin_data['unpaid'].isnull())]

        # merge npwp data with application_pefindo_data
        self.application_pefindo_data['npwp_no'] = self.application_pefindo_data['npwp_no'].str.replace(' ', '').str.replace('.','').str.replace('-','')
        self.application_pefindo_data = pd.merge(self.application_pefindo_data, self.npwp, on=['npwp_no'], how="left")

        # clean application pefindo data
        self.application_pefindo_data = preprocess_lazada.clean_application_pefindo_data(self.application_pefindo_data)
        self.logger.info('Merge admin service and application pefindo data')
        self.clean_data = pd.merge(self.admin_data, self.application_pefindo_data, on='application_id', how='inner')
        return self.clean_data

    def get_clean_transaction_data(self):
        # clean all seller transaction data and merge them
        self.logger.info('Clean old and new seller transaction data')
        self.seller_old_transaction_data = preprocess_lazada.clean_seller_transaction_data(self.seller_old_transaction_data,'old')
        self.seller_new_transaction_data = preprocess_lazada.clean_seller_transaction_data(self.seller_new_transaction_data,'new')
        self.seller_clean_transaction_data = pd.concat([self.seller_old_transaction_data,self.seller_new_transaction_data],axis=0).reset_index(drop=True)
        self.seller_clean_transaction_data = self.seller_clean_transaction_data.drop_duplicates()
        
        self.seller_clean_transaction_data = preprocess_lazada.default_fill_seller_transaction_data(self.seller_clean_transaction_data)
        
        return self.seller_clean_transaction_data
        
    
def main():
    dataset = Dataset()
    dataset.get_clean_dataset()
    
if __name__ == '__main__':
    # find .env conf file automagically
    main()